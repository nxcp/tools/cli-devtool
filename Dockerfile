FROM	alpine:3.15

ARG KUSTOMIZE_VERSION=4.5.2
ARG HELM_VERSION=3.8.0
ARG ETCD_VERSION=3.5.2


ENV USER oper
ENV BASH_ENV "/home/${USER}/.bashrc"

ADD ./resources/cacerts/* /usr/local/share/ca-certificates/
ADD ./resources/dotfiles /tmp/dotfiles
ADD ./resources/etc/ /etc/

RUN \
    # Install software
    apk add --no-cache --virtual=.build-dependencies \
      curl \
      git \
      go \
    && \
    # Install k8split
    go get -u github.com/brendanjryan/k8split && \
    mv ~/go/bin/* /usr/local/bin/ && \
    rm -rf ~/go && \
    \
    # Install minio client
    curl -L https://dl.min.io/client/mc/release/linux-amd64/mc -o /usr/local/bin/mc && \
    chmod +x /usr/local/bin/mc && \
    # Install kubectl ( latest )
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    \
    # Install kustomize
    curl -L https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz -o /tmp/kustomize.tar.gz && \
    tar -C /usr/local/bin/ -xvf /tmp/kustomize.tar.gz && \
    chmod +x /usr/local/bin/kustomize && \
    \
    # Install helm3
    curl -L https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz -o /tmp/helm3.tar.gz && \
    tar -C /tmp -xvf /tmp/helm3.tar.gz && \
    mv /tmp/linux-amd64/helm /usr/local/bin/ && \
    chmod +x /usr/local/bin/helm && \
    \
    # Install yq latest version
    curl -L https://github.com/mikefarah/yq/releases/download/$(curl -s https://github.com/mikefarah/yq/releases/latest | grep -o -E "tag/[^\"]+" | sed 's/tag\///' )/yq_linux_amd64 -o /usr/local/bin/yq && \
    chmod +x /usr/local/bin/yq && \
    \
    # Install etcdctl
    curl -L https://github.com/etcd-io/etcd/releases/download/v${ETCD_VERSION}/etcd-v${ETCD_VERSION}-linux-amd64.tar.gz -o /tmp/etcd.tar.gz && \
    tar -C /tmp -xvf /tmp/etcd.tar.gz --strip-components=1 &&  rm -f /tmp/etcd.tar.gz &&  \
    cp /tmp/etcd* /usr/local/bin/ && \
    \
    # Remove unwanted software
    apk del .build-dependencies && \
    \
    # Install additional tools
    apk add \
      ansible \
      bash \
      ca-certificates \
      curl \
      gettext \
      git \
      git-crypt \
      gnupg \
      go \
      jq \
      openssh-client \
      openssl \
      python3 \
      py-pip \
      sshpass \
      sudo \
      vim \
      && \
    \
    # Install ca-certificate
    update-ca-certificates && \
    \
    # Add user
    addgroup -S ${USER} && \
    adduser -S ${USER} -G ${USER} -h /home/${USER} && \
    \
    # Add sudo
    echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER && \
    chmod 0440 /etc/sudoers.d/$USER \
    \
    # Install dotfiles
    cp -a /tmp/dotfiles/.[^.]* /root; \
    cp -a /tmp/dotfiles/.[^.]* /home/${USER}; \
    chown -R ${USER}:${USER} /home/${USER}; \
    chmod 600 /home/${USER}/.ssh/*; \
    chmod 600 /root/.ssh/*; \
    \
    # Remove tmp and cache
    rm -rf /var/cache/apk/* /tmp/* /var/tmp/*

WORKDIR /home/${USER}
USER ${USER}
CMD [ "/bin/bash" ]