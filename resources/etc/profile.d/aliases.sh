#!/bin/bash

alias ll="ls -l"
alias k="kubectl"
alias vi="vim"
alias python="python3"