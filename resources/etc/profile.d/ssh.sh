#!/bin/bash

#########################################################
# SSH_PASSPHRASE : Base64 encoded passphrase
# SSH_PASSWORD : Base64 encoded password
# SSH_KEY : SSH private key file
# SSH_PUBKEY: SSH public key file
# SSH_CONFIG: SSH configuration file
#########################################################

# SSH_OPTS=""
# SSHPASS_CMD="sshpass -P passphrase -p $(echo $SSH_PASSPHRASE| base64 -d)"

init() {

  SSH_OPTS=""
  SSHPASS_CMD=""
  TMP_PASS="/tmp/$(echo $RANDOM | base64)"
  # Define SSH_OPTS
  if [[ -f $SSH_KEY ]]; then
    SSH_OPTS="$SSH_OPTS -o PreferredAuthentications=publickey -i $SSH_KEY"

    if [[ -n $SSH_PASSPHRASE ]]; then
      echo $SSH_PASSPHRASE | base64 -d > $TMP_PASS 
      SSHPASS_CMD="sshpass -P passphrase -f $TMP_PASS"
    fi
  elif [[ -n $SSH_PASSWORD ]]; then
    echo $SSH_PASSWORD | base64 -d > $TMP_PASS 
    SSH_OPTS="$SSH_OPTS -o PreferredAuthentications=password" 
    SSHPASS_CMD="sshpass -f $TMP_PASS"
  fi

  # Specific SSH_CONFIG file available
  if [[ -f $SSH_CONFIG ]]; then
    SSH_OPTS="$SSH_OPTS -F $SSH_CONFIG"
  fi
  
  SSH_OPTS="$SSH_OPTS -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=ERROR"
}

# Cleanup temp file
cleanup() {
  rm -f $TMP_PASS
  unset TMP_PASS
}

# Proxy function for ssh command
ssh_proxy_cmd() {
  init
  # echo "${SSHPASS_CMD} $(which ssh) ${SSH_OPTS} $@"
  ${SSHPASS_CMD} $(which ssh) ${SSH_OPTS} $@
  cleanup
}

# Proxy function for scp command 
scp_proxy_cmd() {
  init
  # echo "${SSHPASS_CMD} $(which scp) ${SSH_OPTS} $@"
  ${SSHPASS_CMD} $(which scp) ${SSH_OPTS} $@
  cleanup
}

# Proxy function for sftp command 
sftp_proxy_cmd() {
  init
  # echo "${SSHPASS_CMD} $(which sftp) ${SSH_OPTS} $@"
  ${SSHPASS_CMD} $(which sftp) ${SSH_OPTS} $@
  cleanup
}

# Defined aliasess
alias ssh='ssh_proxy_cmd'
alias sftp='sftp_proxy_cmd'
alias scp='scp_proxy_cmd'