# CLI devtools

## Installed software

- kustomize (4.1.3)
- kubectl latest (1.21.1)
- jq latest (1.6)
- yq latest (4.7.1)
- ansible latest 2.10.5
- helm (3.6.0)
- git latest (2.30)
- bash
- python3 (3.8.10)
- urlencode
- openssh-client

## Build

```sh
IMAGE=<IMAGENAME>:<TAG>

docker build -t $IMAGE .
```

## Usage

Mount `kubconfig` file to container then set environment variable `KUBECONFIG` to `kubeconfig` file.

```sh

IMAGE=IMAGE=<IMAGENAME>:<TAG>


docker run -it --rm  \
  --name kubectl \
  -e KUBECONFIG=$KUBECONFIG \
  -v config:/tmp/kubeconfig \
  $IMAGE
```
